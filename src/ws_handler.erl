-module(ws_handler).

-behaviour(cowboy_websocket_handler).

-export([init/3]).
-export([websocket_terminate/3]).
-export([websocket_handle/3]).
-export([websocket_info/3]).
-export([websocket_init/3]).

-include("deps/amqp_client/include/amqp_client.hrl").

-record(state, {channel, connection, queue}).

init(_, _Req, _Opts) ->
    {upgrade, protocol, cowboy_websocket}.

websocket_init(_Transport, Req, _Opts) ->
    {ok, Connection} = amqp_connection:start(#amqp_params_network{host = "rabbitmq"}),
    {ok, Channel}    = amqp_connection:open_channel(Connection),
    #'queue.declare_ok'{queue = Q} = amqp_channel:call(Channel, #'queue.declare'{auto_delete = true}),
    Sub = #'basic.consume'{queue = Q},
    #'basic.consume_ok'{} = amqp_channel:subscribe(Channel, Sub, self()),
    {ok, Req, #state{channel = Channel, connection = Connection, queue = Q}}.


websocket_handle({text, _Msg}, Req, State) ->
    IncomingMessage = jiffy:decode(_Msg, [return_maps]),
    {ok, Meta} = maps:find(<<"meta">>, IncomingMessage),
    {ok, Event} = maps:find(<<"event">>, Meta),
    {ok, Data} = maps:find(<<"data">>, IncomingMessage),
    Reply = websocket_handle_({text, {Event, Data}}, Req, State),
    {reply, {text, Reply}, Req, State}.

websocket_handle_({text, {<<"system.ping">>, _Data}}, Req, State) ->
    jiffy:encode(#{<<"meta">> => #{<<"event">> => <<"system.ping">>}, <<"data">> => <<"pong">>});

websocket_handle_({text, {<<"board.view">>, Data}}, Req, State) ->
    {ok, BoardId} = maps:find(<<"board">>, Data),
    Exchange = << <<"board.">>/binary, BoardId/binary>>,
    RoutingKey = << <<"board.">>/binary, BoardId/binary>>, 
    Declare = #'exchange.declare'{exchange = Exchange},
    ExchangeBind = #'exchange.bind'{source = <<"amq.direct">>, destination = Exchange, routing_key = RoutingKey},
    QueueBind = #'queue.bind'{queue = State#state.queue, exchange = Exchange, routing_key = RoutingKey},
    #'exchange.declare_ok'{} = amqp_channel:call(State#state.channel, Declare),
    #'exchange.bind_ok'{} = amqp_channel:call(State#state.channel, ExchangeBind),
    #'queue.bind_ok'{} = amqp_channel:call(State#state.channel, QueueBind),
    <<"ok">>.

websocket_info({#'basic.deliver'{delivery_tag = _Tag}, Content}, Req, State) -> 
    #amqp_msg{payload = Payload} = Content,
    amqp_channel:cast(State#state.channel, #'basic.ack'{delivery_tag = _Tag}),
    {reply, {text, Payload}, Req, State};

websocket_info(_Info, Req, State) -> 
    {ok, Req, State}.

websocket_terminate(_Reason, _Req, State) ->
    amqp_channel:close(State#state.channel),
    amqp_connection:close(State#state.connection),
    ok.

