-module(wsserver_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    Dispatch = cowboy_router:compile([
        {'_', [
                {"/", demo_handler, []},
                {"/ws", ws_handler, []}
            ]
        }
    ]),
    {ok, _} = cowboy:start_http(http_listener, 100, 
                      [{port, 80}],
                      [{env, [{dispatch, Dispatch}]}]
                     ),
    wsserver_sup:start_link().

stop(_State) ->
    ok.
